package com.hmdp.common;

public interface Constants {
    public static final String IMAGE_UPLOAD_DIR = "/usr/local/var/www/hmdp/imgs/";
    /**
     * 随机名字生成 ：名字中的前缀
     */
    public static final String USER_NICK_NAME_PREFIX = "ss_";

    /**
     * 短信验证码有效期间 5分钟
     */
    public static final Long LOGIN_CODE_TTL = 5L;

    /**
     * 用户登录凭证 和 用户登录信息 有效期 (put(token,userInfo))
     */


    /**
     * 商品信息缓存有效期为30分钟
     */
    public static final Long CACHE_SHOP_TTL = 30L;


    /**
     * 缓存空对象-有效期
     */
    public static final Long CACHE_NULL_TTL = 2L;



    public static final Long LOGIN_TOKEN_TTL = 30L;


    public static final String BLOG_LIKED_KEY = "blog:liked:";



    public static final Long LOCK_SHOP_TTL = 10L;

    public static final int DEFAULT_PAGE_SIZE = 5;
    public static final int MAX_PAGE_SIZE = 10;

    public static final String FEED_KEY = "feed:";
    public static final String SHOP_GEO_KEY = "shop:geo:";
    public static final String USER_SIGN_KEY = "sign:";
}
