package com.hmdp.common;

import java.time.LocalDateTime;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Auther: 公众号/B站：是叶十三
 * @Date: 2022/11/15
 * @Description: com.hmdp.common
 * @version: 1.0
 */
@Data
@Accessors(chain = true)
public class RedisData {
    /**
     * 过期时间
     */
    private LocalDateTime expireTime;

    /**
     * 数据
     */
    private Object data;

}
