package com.hmdp.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class R {
    private Boolean success;
    private String errorMsg;
    private Object data;
    private Long total;

    /**
     * 成功：什么都不传递
     * @return
     */
    public static R ok(){
        return new R(true, null, null, null);
    }

    /**
     * 成功：传递一个带有数据data的对象
     * @param data
     * @return
     */
    public static R ok(Object data){
        return new R(true, null, data, null);
    }

    /**
     * 成功：传递集合类型
     * @param data
     * @param total
     * @return
     */
    public static R ok(List<?> data, Long total){
        return new R(true, null, data, total);
    }

    /**
     * 失败：传递原因
     * @param errorMsg 失败的原因
     * @return
     */
    public static R fail(String errorMsg){
        return new R(false, errorMsg, null, null);
    }

}
