package com.hmdp.common;

import java.util.List;

import lombok.Data;

/**
 * 滚动分页
 */
@Data
public class ScrollResult {
    private List<?> list;
    private Long minTime;
    private Integer offset;
}