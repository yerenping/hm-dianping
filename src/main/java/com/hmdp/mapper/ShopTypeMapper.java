package com.hmdp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmdp.entity.ShopType;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 公众号：是叶十三
 * @since 2021-12-22
 */

public interface ShopTypeMapper extends BaseMapper<ShopType> {

}
