package com.hmdp.mapper;

import com.hmdp.entity.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 公众号：是叶十三
 * @since 2021-12-24
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
