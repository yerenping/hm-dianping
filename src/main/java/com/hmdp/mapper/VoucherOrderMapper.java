package com.hmdp.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmdp.entity.VoucherOrder;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 公众号：是叶十三
 * @since 2021-12-22
 */
@Mapper
public interface VoucherOrderMapper extends BaseMapper<VoucherOrder> {
    /**
     * 添加一比订单记录
     * @param voucherOrder
     * @return
     */
    int addOrder(VoucherOrder voucherOrder);

    /**
     * 根据userId 查订单id
     * @param setUserId
     * @return
     */
    Long selectByUserId(VoucherOrder setUserId);
}
