package com.hmdp.mapper;

import com.hmdp.entity.Follow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 公众号：是叶十三
 * @since 2021-12-22
 */
public interface FollowMapper extends BaseMapper<Follow> {

}
