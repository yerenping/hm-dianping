package com.hmdp.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hmdp.common.R;
import com.hmdp.service.IFollowService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 公众号：是叶十三
 * @since 2021-12-22
 */
@RestController
@RequestMapping("/follow")
public class FollowController {
    private final IFollowService followService;

    @Autowired
    public FollowController(IFollowService followService) {
        this.followService = followService;
    }

    @PutMapping("/{id}/{isFollow}")
    public R follow(@PathVariable("id") Long followUserId, @PathVariable("isFollow") Boolean isFollow) {
        return followService.follow(followUserId, isFollow);
    }

    @GetMapping("/or/not/{id}")
    public R isFollow(@PathVariable("id") Long followUserId) {
        return followService.isFollow(followUserId);
    }

    @GetMapping("/common/{id}")
    public R followCommons(@PathVariable("id") Long id){
        return followService.followCommons(id);
    }

}
