package com.hmdp.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hmdp.common.R;
import com.hmdp.service.IVoucherOrderService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 公众号：是叶十三
 * @since 2021-12-22
 */
@RestController
@RequestMapping("/voucher-order")
public class VoucherOrderController {

    public final IVoucherOrderService voucherOrderService;

    @Autowired
    public VoucherOrderController(IVoucherOrderService voucherOrderService) {
        this.voucherOrderService = voucherOrderService;
    }

    @PostMapping("seckill/{id}")
    public R seckillVoucher(@PathVariable("id") Long voucherId){
//        return  voucherOrderService.seckillVoucher(voucherId);
//        return  voucherOrderService.seckillVoucherForOneBuyOne(voucherId);


        return  voucherOrderService.seckillVoucherForOneBuyOnePessimisticLock(voucherId);
    }
}
