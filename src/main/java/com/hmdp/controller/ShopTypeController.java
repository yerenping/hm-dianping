package com.hmdp.controller;


import com.hmdp.common.R;
import com.hmdp.service.IShopTypeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 公众号：是叶十三
 * @since 2021-12-22
 */
@RestController
@RequestMapping("/shop-type")
public class ShopTypeController {
    @Resource
    private IShopTypeService shopTypeService;

    @GetMapping("list")
    public R queryTypeList() {
//        List<ShopType> typeList = typeService.query().orderByAsc("sort").list();
//        return R.ok(typeList);
        return shopTypeService.showList();

    }
}
