package com.hmdp.service;

import javax.servlet.http.HttpSession;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmdp.common.R;
import com.hmdp.dto.LoginFormDTO;
import com.hmdp.entity.Blog;
import com.hmdp.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 公众号：是叶十三
 * @since 2021-12-22
 */
public interface IUserService extends IService<User> {

    R sendCode(String phone, HttpSession session);

    R login(LoginFormDTO loginForm, HttpSession session);

}
