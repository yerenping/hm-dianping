package com.hmdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmdp.common.R;
import com.hmdp.entity.Blog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 公众号：是叶十三
 * @since 2021-12-22
 */
public interface IBlogService extends IService<Blog> {
    R queryHotBlog(Integer current);

    R queryBlogById(Long id);

    R likeBlog(Long id);

    R queryBlogLikes(Long id);

    R saveBlog(Blog blog);

    R queryBlogOfFollow(Long max, Integer offset);
}
