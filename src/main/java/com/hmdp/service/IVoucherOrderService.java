package com.hmdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmdp.common.R;
import com.hmdp.entity.VoucherOrder;

/**
 * @author: YeRenping
 * @Date: 2023/2/19
 * @Description:秒杀相关
 */
public interface IVoucherOrderService extends IService<VoucherOrder> {

    public R seckillVoucher(Long voucherId);

    public R seckillVoucherForOneBuyOne(Long voucherId);

    public R seckillVoucherForOneBuyOnePessimisticLock(Long voucherId);

    R createOrder(Long voucherId, Integer stock, Long userId);
}
