package com.hmdp.service;

import com.hmdp.common.R;
import com.hmdp.entity.Shop;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 公众号：是叶十三
 * @since 2021-12-22
 */
public interface IShopService extends IService<Shop> {

    R selectByIdV1(Long id);

    R selectByIdV2(Long id);

    R selectByIdV3(Long id);

    R selectByIdV4(Long id);
}
