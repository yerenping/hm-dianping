package com.hmdp.service;

import com.hmdp.common.R;
import com.hmdp.entity.Voucher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 公众号：是叶十三
 * @since 2021-12-22
 */
public interface IVoucherService extends IService<Voucher> {

    R queryVoucherOfShop(Long shopId);

    void addSeckillVoucher(Voucher voucher);


}
