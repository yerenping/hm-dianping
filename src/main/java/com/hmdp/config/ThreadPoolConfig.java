package com.hmdp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
// 启用定时任务
@EnableScheduling
// 锁
@EnableAsync
public class ThreadPoolConfig {

}