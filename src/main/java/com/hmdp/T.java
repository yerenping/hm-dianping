package com.hmdp;

import java.util.Date;

/**
 * @author: YeRenping
 * @Date: 2023/2/28
 * @Description:
 */
public class T {
    public static void main(String[] args) {
        Date expiration = new Date(System.currentTimeMillis() + 3600 * 24 * 1000L);
        System.out.println(expiration);
    }
}
