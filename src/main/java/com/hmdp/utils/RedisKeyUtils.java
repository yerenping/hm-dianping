package com.hmdp.utils;

public class RedisKeyUtils {
    // 分隔符
    private static final String SPLIT = ":";
    // 构造验证码的前缀
    public static final String PREFIX_LOGIN_CODE = "login:code";
    // 用户登录信息
    public static final String PREFIX_LOGIN_INFO = "login:token";
    // 缓存商品信息
    public static final String PREFIX_CACHE_SHOP = "cache:shop:%d";
    // 缓存店铺类型列表
    public static final String PREFIX_CACHE_SHOP_TYPE = "cache:shop_type_list";
    // 互斥锁
    public static final String PREFIX_LOCK_SHOP_KEY = "lock:shop";
    // 秒杀库存
    public static final String PREFIX_SECKILL_STOCK = "seckill:stock";



    public static final String SECKILL_STOCK_KEY = "seckill:stock:";
    public static final String BLOG_LIKED_KEY = "blog:liked:";
    public static final String FEED_KEY = "feed:";
    public static final String SHOP_GEO_KEY = "shop:geo:";
    public static final String USER_SIGN_KEY = "sign:";


    private static final String PREFIX_ENTITY_LIKE = "like:entity";
    private static final String PREFIX_USER_LIKE = "like:user";
    private static final String PREFIX_FOLLOWEE = "followee";
    private static final String PREFIX_FOLLOWER = "follower";
    private static final String PREFIX_KAPTCHA = "kaptcha";
    private static final String PREFIX_TICKET = "ticket";
    private static final String PREFIX_USER = "user";
    private static final String PREFIX_UV = "uv";
    private static final String PREFIX_DAU = "dau";
    private static final String PREFIX_POST = "post";
    private static final String PREFIX_RATE_LIMIT_PUBLISH = "rate:limit:publish";

    /**
     * 用户登录验证码
     *
     * @param phone
     * @return
     */
    public static String getLoginCodeKey(String phone) {
        return PREFIX_LOGIN_CODE + SPLIT + phone;
    }

    /**
     * 存储用户登录信息
     *
     * @param token
     * @return
     */
    public static String getUserInfoKey(String token) {
        return PREFIX_LOGIN_INFO + SPLIT + token;
    }

    /**
     * 根据id
     * @param id
     * @return
     */
    public static  String getShopInfoKey(Long id){
        return String.format(PREFIX_CACHE_SHOP ,id) ;
    }

    /**
     * 缓存所有商品类型
     * @return
     */
    public static  String getShopTypeListKey(){
        return PREFIX_CACHE_SHOP_TYPE + SPLIT;
    }
    /**
     * 获取互斥锁redisKey
     * @return
     */
    public static  String getShopLockKey(Long id){
        return PREFIX_LOCK_SHOP_KEY + SPLIT + id;
    }
    /**
     * 获取秒杀库存
     * @return
     */
    public static  String getSecillStockKey(Long id){
        return PREFIX_SECKILL_STOCK + SPLIT + id;
    }

//    /**
//     * 某个实体的赞 like:entity:entityType:entityId->set(userId)
//     * 这是为什么用set存点赞的个数呢？ 因为方便后期项目更新，需要获取点赞人的信息
//     * 使用set集合 可以通过方法统计id的个数，从而得到点赞的个数
//     * @param entityType
//     * @param entityId
//     * @return
//     */
//    public static String getEntityLikeKey(int entityType, int entityId) {
//        return PREFIX_ENTITY_LIKE + SPLIT + entityType + SPLIT + entityId;
//    }
//
//    /**
//     * 某个用户的赞 like:user:userId -> int
//     *
//     * @param userId
//     * @return
//     */
//    public static String getUserLikeKey(int userId) {
//        return PREFIX_USER_LIKE + SPLIT + userId;
//    }
//
//    /**
//     * 某个用户 关注 的实体
//     *
//     * followee:userId:entityType -> zset(entityId, now)
//     * zset(实体id，时间)
//     * 按照时间进行排序，时间越早排在越前面
//     * @param userId
//     * @param entityType
//     * @return
//     */
//    public static String getFolloweeKey(int userId, int entityType) {
//        return PREFIX_FOLLOWEE + SPLIT + userId + SPLIT + entityType;
//    }
//
//    /**
//     * 某个实体拥有的粉丝 follower:entityType:entityId -> zset(userId, now)
//     *
//     * @param entityType
//     * @param entityId
//     * @return
//     */
//    public static String getFollowerKey(int entityType, int entityId) {
//        return PREFIX_FOLLOWER + SPLIT + entityType + SPLIT + entityId;
//    }
//
//    /**
//     * 登录验证码
//     *
//     * @param owner 服务器发给客户端的临时凭证
//     * @return
//     */
//    public static String getKaptchaKey(String owner) {
//        return PREFIX_KAPTCHA + SPLIT + owner;
//    }
//
//    /**
//     * 登录的凭证
//     *
//     * @param ticket
//     * @return
//     */
//    public static String getTicketKey(String ticket) {
//        return PREFIX_TICKET + SPLIT + ticket;
//    }
//
//    /**
//     * 用户
//     *
//     * @param userId
//     * @return
//     */
//    public static String getUserKey(int userId) {
//        return PREFIX_USER + SPLIT + userId;
//    }
//
//    /**
//     * 单日UV
//     *
//     * @param date
//     * @return
//     */
//    public static String getUvKey(String date) {
//        return PREFIX_UV + SPLIT + date;
//    }
//
//    /**
//     * 区间UV
//     *
//     * @param startDate
//     * @param endDate
//     * @return
//     */
//    public static String getUvKey(String startDate, String endDate) {
//        return PREFIX_UV + SPLIT + startDate + SPLIT + endDate;
//    }
//
//    /**
//     * 单日DAU
//     *
//     * @param date
//     * @return
//     */
//    public static String getDauKey(String date) {
//        return PREFIX_DAU + SPLIT + date;
//    }
//
//    /**
//     * 区间DAU
//     *
//     * @param startDate
//     * @param endDate
//     * @return
//     */
//    public static String getDauKey(String startDate, String endDate) {
//        return PREFIX_DAU + SPLIT + startDate + SPLIT + endDate;
//    }
//
//    /**
//     * 帖子分数
//     *
//     * @return
//     */
//    public static String getPostScoreKey() {
//        return PREFIX_POST + SPLIT + "score";
//    }
//

}
