package com.hmdp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.hmdp.service.impl.ShopServiceImpl;

/**
 * @Auther: 公众号/B站：是叶十三
 * @Date: 2022/11/15
 * @Description: com.hmdp
 * @version: 1.0
 */
@SpringBootTest
class RedisTestSaveShop {
    @Autowired
    private ShopServiceImpl shopServiceImpl;

    @Test
    void testSaveShop() throws InterruptedException {
        shopServiceImpl.saveShop2Redis(1L, 10L);
    }
}
