package com.hmdp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @author: YeRenping
 * @Date: 2022/12/25
 * @Description: com.hmdp
 */
@SpringBootTest
public class HperLogLogTest {
    private final StringRedisTemplate redisTemplate;

    @Autowired
    public HperLogLogTest(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Test
    void testHyperLogLog() {
        String[] values = new String[1000];
        int j = 0;
        for (int i = 0; i < 1000000; i++) {
            j = i % 1000;
            values[j] = "user_" + i;
            if(j == 999){
                // 发送到Redis
                redisTemplate.opsForHyperLogLog().add("hl2", values);
            }
        }
        // 统计数量
        Long count = redisTemplate.opsForHyperLogLog().size("hl2");
        System.out.println("count = " + count);
    }

}
