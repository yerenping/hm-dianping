package com.hmdp.mapper;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.hmdp.entity.SeckillVoucher;

/**
 * @author: YeRenping
 * @Date: 2023/2/20
 * @Description:
 */
@SpringBootTest
@Slf4j
class SeckillVoucherMapperTest {

    @Resource
    SeckillVoucherMapper seckillVoucherMapper;

    @Test
    void updateStock() {
        int i = seckillVoucherMapper.updateStock(new SeckillVoucher().setVoucherId(1L));
        log.debug("{}",i);
    }
}