package com.hmdp;


import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.hmdp.common.RedisIdWorker;

/**
 * @Auther: 公众号/B站：是叶十三
 * @Date: 2022/11/15
 * @Description: com.hmdp
 * @version: 1.0
 */
@Slf4j
@SpringBootTest
public class RedisIdWorkerTest {
    @Autowired
    private RedisIdWorker redisWorker;

    // 创建jdk线程池
    private ExecutorService es =  Executors.newFixedThreadPool(500);

    @Test
    public void test() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(300);
        Runnable task = ()->{
            for(int i = 0; i < 100; i ++){
                long id = redisWorker.nextId("order");
                System.out.println("id：" + id);
            }
            countDownLatch.countDown();
        };
        long start = System.currentTimeMillis();
        for (int i = 0; i < 300; i++) {
            es.submit(task);
        }
        countDownLatch.await();
        long end = System.currentTimeMillis();




        System.out.println("总共用时:" + (end - start));
    }
}
